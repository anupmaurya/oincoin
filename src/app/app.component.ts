import { Component, ViewChild } from '@angular/core';
import { Platform,  Nav, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AppServiceProvider } from '../providers/appService/appService';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage: any = 'LoginPage';
  isparent = null;
  //rootPage:any='ChildernDashboardPage';
  pages = [];
  constructor(platform: Platform, statusBar: StatusBar,
    public utility: AppServiceProvider,
    public events: Events,
    splashScreen: SplashScreen) {

      this.events.subscribe('loadmenu',()=>{
        this.loadMenu();
      })

      this.utility.OnloadLocal().then(res=>{
        if(this.utility.localdata.user_profile){
          this.isparent = this.utility.localdata.isparent
          if(this.utility.localdata.isparent){
            this.nav.setRoot('ChildernListPage');
          }else if(this.utility.localdata.selectChild){
            this.nav.setRoot('ChildernDashboardPage');
          }else{
            this.nav.setRoot('HomePage');
          }
          this.loadMenu();
        }else{
          this.nav.setRoot('LoginPage');
        }
      },error=>{})
      
  
    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
    });
   
  }


  loadMenu = () =>{
    let tempPage = [
      { title: 'Home', component: 'ChildernListPage', isparent: true },
      { title: 'Home', component: 'ChildernDashboardPage', isparent: false },
      { title: 'Show Task', component: 'ScheduletaskPage', isparent: false },
      { title: 'logout', component: ' ',isparent: 1 }
    ]
    
    this.pages = [];

    for(var i=0;i<tempPage.length;i++){
      console.log(tempPage[i].isparent + "==" + this.isparent)
        if(tempPage[i].isparent===this.isparent || tempPage[i].isparent===1 ){
          this.pages.push(tempPage[i]);
        }
    }
  }

  openPage(p){
    if(p.title=="logout"){
      console.log(p);
      this.utility.Logout();
      this.nav.setRoot('LoginPage');
    }else{
      this.nav.setRoot(p.component);
    }
  }

}

