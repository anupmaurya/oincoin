import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule} from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from "@angular/http";
import { IonicStorageModule } from '@ionic/storage';
import { Camera } from '@ionic-native/camera';

import { MyApp } from './app.component';

import { LoginPageModule } from '../pages/login/login.module';
import { SignupPageModule } from '../pages/signup/signup.module';
import { RegisterPageModule } from '../pages/register/register.module';
import { ChildernDashboardPageModule } from '../pages/childern-dashboard/childern-dashboard.module';
import { ChildernListPageModule } from '../pages/childern-list/childern-list.module';
import { CustomegolePageModule } from '../pages/customegole/customegole.module';
import { CustometaskPageModule } from '../pages/custometask/custometask.module';
import { GoalPageModule } from '../pages/goal/goal.module';
import { LoginoptionPageModule } from '../pages/loginoption/loginoption.module';
import { Ragistersetep2PageModule } from '../pages/ragistersetep2/ragistersetep2.module';
import { Ragistersetep3PageModule } from '../pages/ragistersetep3/ragistersetep3.module';
import { Ragistersetep4PageModule } from '../pages/ragistersetep4/ragistersetep4.module';
import { Ragistersetep5PageModule } from '../pages/ragistersetep5/ragistersetep5.module'
import { OtherloginPageModule } from '../pages/otherlogin/otherlogin.module'
import { CredantialloginPageModule } from '../pages/credantiallogin/credantiallogin.module'
import { ApiServiceProvider } from '../providers/apiService/apiService';
import { AppServiceProvider } from '../providers/appService/appService';
import { AddgoalPageModule } from '../pages/addgoal/addgoal.module'
import { ObjectiveeditPageModule } from '../pages/objectiveedit/objectiveedit.module';
import { CheckearningPageModule } from '../pages/checkearning//checkearning.module'
import { HomePageModule } from '../pages/home/home.module';
import { ScheduletaskPageModule } from '../pages/scheduletask/scheduletask.module';
import { DatePicker } from '@ionic-native/date-picker';

// import { ComponentsModule } from '../components/components.module'

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    LoginPageModule,
    SignupPageModule,
    RegisterPageModule,
    ChildernDashboardPageModule,
    ChildernListPageModule,
    CustometaskPageModule,
    CustomegolePageModule,
    GoalPageModule,
    LoginoptionPageModule,
    Ragistersetep2PageModule,
    Ragistersetep3PageModule,
    Ragistersetep4PageModule,
    Ragistersetep5PageModule,
    OtherloginPageModule,
    CredantialloginPageModule,
    AddgoalPageModule,
    ObjectiveeditPageModule,
    CheckearningPageModule,
    HttpModule,
    BrowserModule,
    HomePageModule,
    ScheduletaskPageModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApiServiceProvider,
    AppServiceProvider,
    DatePicker
  ]
})
export class AppModule {}

