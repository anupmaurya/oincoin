import { Component,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'weekendlist',
  templateUrl: 'weekendlist.html'
})
export class WeekendlistComponent {

  @Output() selectweek : EventEmitter<any> = new EventEmitter<any>();

  text: string;
  Quando: string;
  constructor() {
    console.log('Hello WeekendlistComponent Component');
    this.text = 'Hello World';
  }

  onChangeweek(e){
    this.selectweek.emit({selectweek : e})
  }

}
