import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/apiService/apiService';
import { AppServiceProvider } from '../../providers/appService/appService'
@IonicPage()
@Component({
  selector: 'page-addgoal',
  templateUrl: 'addgoal.html',
})
export class AddgoalPage {
  public SelectGoal = { goal_name: '',child_goal_id : null };
  public listGoal = [];
  public defaultGoals = [
    { goal_icon: 'boll.png', goal_name: 'Boll', goal_value: '', id: 1 },
    { goal_icon: 'doll.png', goal_name: 'Doll', goal_value: '', id: 2 },
    { goal_icon: 'Skateboard.png', goal_name: 'Skateboard', goal_value: '', id: 3 },
    { goal_icon: 'videogame.png', goal_name: 'Video game', goal_value: '', id: 4 },
  ]
  public user_id: number;
  constructor(public navCtrl: NavController,
    public apiServiceProvider: ApiServiceProvider,
    public events: Events,
    public utility: AppServiceProvider,
    public navParams: NavParams) {
    
    console.log(this.navParams.get('parm'));
    let parm = this.navParams.get('parm');
    if(parm){
      this.SelectGoal = parm;
    }
    this.events.subscribe('SelectGoal', (payload, GoalList?) => {
      this.SelectGoal = payload
      console.log(this.SelectGoal)
      if (GoalList) {
        console.log(GoalList)
         this.defaultGoals = GoalList;
         this.utility.ShowLoader();
         this.onload();
      }
    });
    console.log(this.utility.localdata.user_profile.id)
    this.user_id = this.utility.localdata.user_profile.id
    this.loadApi();
  }

  ionViewDidLoad() {
  }

  goToback() {
    this.navCtrl.pop();
  }

  createCustom(item) {
    this.navCtrl.push('CustomegolePage', { Goal: item, listGoal: this.defaultGoals });
  }

  goToNext() {
    console.log(this.SelectGoal.goal_name);
    if(this.navParams.get('parm')){
      this.SelectGoal.child_goal_id = this.navParams.get('parm').child_goal_id  
        console.log(this.SelectGoal);
        console.log(this.navParams.get('parm'));
    }else{
      if(this.SelectGoal.goal_name!=""){
        this.navCtrl.push('AddtaskPage',{selectGoal:this.SelectGoal})
      }else{
        this.utility.presentAlert('Message','Please select at least one Goal','Ok');
      }
    }
    
  }

  goToBack() {
    this.navCtrl.pop();
  }

  loadApi(){
    this.utility.ShowLoader();
    this.apiServiceProvider.callApi('getGoal', 'post', { user_id: this.user_id }).subscribe(res => {
      console.log(res);
      let listGoal = res.success
      for (let i = 0; i < listGoal.length; i++) {
        if (listGoal[i].id != 1 && listGoal[i].id != 2 && listGoal[i].id != 3 && listGoal[i].id != 4) {
          console.log(listGoal[i])
          this.defaultGoals.push(listGoal[i]);
        }
      }
      this.onload();
    }, error => {
      console.log(error);
    })
  }

  onload() {
      let data = this.defaultGoals
      this.listGoal = [];
      let j = 0
      var k = 0
      let datalength = Math.round(data.length / 3);
      if ((data.length / 3) % 1 === 0) {
        console.log(true);
      } else {
        datalength = datalength + 1;
      }
      console.log(datalength)
      for (var i = 0; i < datalength; i++) {
        j = j + 3;
        let tempkey: any = [];
        for (k; k < j; k++) {
          // let pass1 = data[k];
          // let pass2;
          // let pass3;
          //let tempdata;
          if (data[k]) {
            tempkey.push(data[k]);
          }
        }
        if (tempkey.length != 0) {
          this.listGoal.push(tempkey)
        }
        k = j;
      }
      this.utility.HideLoader()
  }
}

