import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/apiService/apiService';
import { AppServiceProvider } from '../../providers/appService/appService';

@IonicPage()
@Component({
  selector: 'page-addtask',
  templateUrl: 'addtask.html',
})
export class AddtaskPage {

  public defaultTask = [
    { task_name: 'ARRUMAR O QUARTO', day_of_week: '', periodicity_type: 'W', isSelect: false, id: 1, task_id: 1 },
    { task_name: 'lavar os pratos', day_of_week: '', periodicity_type: 'W', isSelect: false, id: 2, task_id: 2, },
    { task_name: 'fazer lição de casa', day_of_week: '', periodicity_type: 'W', isSelect: false, id: 3, task_id: 3 },
  ];
  public listTask = [];
  public selectGoal;
  public user_id;
  constructor(public navCtrl: NavController,
    public events: Events,
    public apiServiceProvider: ApiServiceProvider,
    public utility: AppServiceProvider,
    public navParams: NavParams) {
    this.selectGoal = this.navParams.get('selectGoal');
    this.events.subscribe('SelectTask', (payload) => this.bindTask(payload));
    this.onload()
    this.loalApi()
    this.user_id = this.utility.localdata.user_profile.id;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddtaskPage');
  }

  createCustom(item) {
    let count = 1;
    for (let i = 0; i < this.defaultTask.length; i++) {
      if (this.defaultTask[i].isSelect) {
        count++
      }
    }

    if (count <= 3) {
      this.navCtrl.push('CustometaskPage', { selectTask: item, listTask: this.defaultTask });
    } else {
      this.utility.presentAlert('Message', 'You have selected three task', 'Done');
    }
  }

  addTask(item) {
    if (item.isSelect) {
      item.isSelect = false;
    } else {
      this.createCustom(item)
    }
  }

  limiofTask() {
  }

  goToback() {
    this.navCtrl.pop();
  }

  goTonext() {
    //this.navCtrl.push('ChildernDashboardPage');
    let selectTask = [];
    for (let i = 0; i < this.defaultTask.length; i++) {
      if (this.defaultTask[i].isSelect == true) {
        this.defaultTask[i].task_id = this.defaultTask[i].id
        selectTask.push(this.defaultTask[i]);
      }
    }
    console.log(selectTask);
    console.log(this.selectGoal);
    const payload = {
      "user_id": this.user_id,
      "tasks": selectTask,
      "children_id": this.utility.localdata.selectChild.id,
      ...this.selectGoal
    }
    console.log(payload);
    this.apiServiceProvider.callApi('assignGoalAndTaskToChildren', 'post', payload).subscribe(res => {
      this.navCtrl.setRoot('ChildernDashboardPage'); ``
    }, error => {
      console.log(error);

    })
  }

  getweekname(id) {
    let result;
    switch (id) {
      case '1':
        result = 'Segunda-feira';
        break;
      case '2':
        result = 'Terça-feira';
        break;
      case '3':
        result = 'Quarta-feira';
        break;
      case '4':
        result = 'Quinta-feira';
      case '5':
        result = 'Sexta-feira';
        break;
      case '6':
        result = 'Sábado';
        break;
      case '7':
        result = 'Domingo';
        break;
      default:
        result = '';
    }
    return result;

  }

  bindTask(payload) {
    if (payload.user_id) {
      let selectTask = payload.custom_task[0];
      for (let i = 0; i < this.defaultTask.length; i++) {
        if (selectTask.id == this.defaultTask[i].id) {
          this.defaultTask[i] = selectTask;
        }
      }
    } else {
      this.defaultTask = payload;
    }

    this.onload();
  }

  onload() {
    this.listTask = [];
    let data = this.defaultTask
    let j = 0
    var k = 0
    let datalength = Math.round(data.length / 3);
    if ((data.length / 3) % 1 === 0) {
      console.log(true);
    } else {
      datalength = datalength + 1;
    }
    console.log(datalength)
    for (var i = 0; i < datalength; i++) {
      j = j + 3;
      let tempkey: any = [];
      for (k; k < j; k++) {
        // let pass1 = data[k];
        // let pass2;
        // let pass3;
        // let tempdata;
        if (data[k]) {
          tempkey.push(data[k]);
        }
      }
      if (tempkey.length != 0) {
        this.listTask.push(tempkey)
      }
      k = j;
    }
    console.log(this.listTask);
    if (this.navParams.get('selectGoal')) {
      this.selectGoal = this.navParams.get('selectGoal')
    }
  }

  loalApi() {
    console.log(this.utility.localdata.user_profile.id)
    this.user_id = this.utility.localdata.user_profile.id;
    this.apiServiceProvider.callApi('getTask', 'post', { user_id: this.user_id }).subscribe(res => {
      let listtask = res.success;
      for (let i = 0; i < listtask.length; i++) {
        if (listtask[i].id != 1 && listtask[i].id != 2 && listtask[i].id != 3) {
          this.defaultTask.push(listtask[i])
        }
        this.onload();
      }
    }, error => {
      console.log(error);
    })
  }
}
