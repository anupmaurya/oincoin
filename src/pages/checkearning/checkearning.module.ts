import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CheckearningPage } from './checkearning';

@NgModule({
  declarations: [
    CheckearningPage,
  ],
  imports: [
    IonicPageModule.forChild(CheckearningPage),
  ],
})
export class CheckearningPageModule {}
