import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the CheckearningPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-checkearning',
  templateUrl: 'checkearning.html',
})
export class CheckearningPage {

  public data = {
    children_id : '',
    children_name : '',
    child_earn : 0,
    goal_icon : '',
    gender : '',
    goal_id : '',
    goal_value : '0',
    number_of_months : '',
    created_at : '',
    goal_name : '',
    ischild_new : true,
    isparent : true,
    child_progress :'0%',
    panding_earn : '0'
  }
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.data = this.navParams.get('data');
  }

}
