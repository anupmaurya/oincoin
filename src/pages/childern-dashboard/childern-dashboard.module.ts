import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChildernDashboardPage } from './childern-dashboard';

@NgModule({
  declarations: [
    ChildernDashboardPage,
  ],
  imports: [
    IonicPageModule.forChild(ChildernDashboardPage),
  ],
})
export class ChildernDashboardPageModule {}
