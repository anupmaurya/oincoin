import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/apiService/apiService';
import { AppServiceProvider } from '../../providers/appService/appService';

@IonicPage()
@Component({
  selector: 'page-childern-dashboard',
  templateUrl: 'childern-dashboard.html',
})

export class ChildernDashboardPage {
    
  public data = {
      children_id : '',
      children_name : '',
      child_earn : 0,
      goal_icon : '',
      gender : '',
      goal_id : '',
      goal_value : '0',
      number_of_months : '',
      created_at : '',
      goal_name : '',
      ischild_new : true,
      isparent : true,
      child_progress :'0%',
      panding_earn : '0'
    }
  
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public utility : AppServiceProvider,public events: Events,
    public apiServiceProvider : ApiServiceProvider) {
    this.ionOnloads()    
    }

  ionOnloads(){
    if(this.utility.localdata.selectChild){
      let payload = {
        children_id : this.utility.localdata.selectChild.id
      }
      this.utility.ShowLoader();
      this.apiServiceProvider.callApi('getDashboardData','post',payload).subscribe(res=>{
        if(res.success.length==0){
          console.log('is false');
          this.data.ischild_new = true;
          this.data.children_name = this.utility.localdata.selectChild.name;
          this.data.gender = this.utility.localdata.selectChild.gender;
          this.data.isparent = this.utility.localdata.isparent;
          this.data.child_earn = 0;
        }else{
          this.data.ischild_new = false;
          this.data =  res.success[0];
          this.data.isparent = this.utility.localdata.isparent;
        }
        this.utility.localdata.childinfo = this.data;
        this.utility.SetLocalStore('childinfo',this.data);
        this.utility.HideLoader();
      },error=>{
        this.utility.HideLoader();
        console.log(error); 
      })
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChildernDashboardPage');
  }

  addGoal(){
    this.navCtrl.push('AddgoalPage');
  }

  gotoScheduletask(){
    this.navCtrl.push('ScheduletaskPage',{parm:this.data})
  }

  GOChildInfo(){
      this.navCtrl.push('CheckearningPage',{data : this.data});
  }

  goChildObjetic(){
    this.navCtrl.push('ObjectiveeditPage',{data : this.data});
  }


 
}
