import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChildernListPage } from './childern-list';

@NgModule({
  declarations: [
    ChildernListPage,
  ],
  imports: [
    IonicPageModule.forChild(ChildernListPage),
  ],
})
export class ChildernListPageModule {}
