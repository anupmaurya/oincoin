import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/apiService/apiService';
import { AppServiceProvider } from '../../providers/appService/appService';

@IonicPage()
@Component({
  selector: 'page-childern-list',
  templateUrl: 'childern-list.html',
})
export class ChildernListPage {

  public data = {
    children_data : [],
    children_list : [],
    responsible_name : '',
    facebook_id : '',
    access_code : '',
    phone_number : '',
    isparent : false
  }

  public boy = './assets/imgs/04 - crianca_menino.png';
  public girl = './assets/imgs/04 - crianca_menina.png';

  constructor(public navCtrl: NavController, 
    public utility : AppServiceProvider,
    public events: Events,
    public apiServiceProvider : ApiServiceProvider,
    public navParams: NavParams) {
      try{

        console.log(this.utility.localdata.user_profile);
        if(this.utility.localdata.user_profile){
          let data = this.utility.localdata.user_profile.children_data;
          console.log(this.utility.localdata.isparent)
          this.data = this.utility.localdata.user_profile;
          this.data.isparent = this.utility.localdata.isparent
          this.data.children_list = [];
          console.log(data);
          let j = 0
          var k =0
          let datalength  = Math.round(data.length/2);
          console.log(datalength)
          for(var i=0;i<datalength;i++){
            j=j+2;
            for(k;k<j;k++){
                let pass1 = data[k];
                let pass2;
                let tempdata;
                k++
                if(data[k]){
                  pass2 = data[k];
                  tempdata = [pass1,pass2]
                }else{
                  tempdata = [pass1]
                }
              this.data.children_list.push(tempdata) 
            }
            k=j;
          }
          this.events.publish('loadmenu');
         }
     }catch(e){};
  }


  ionViewDidLoad() {
    console.log(this.utility.localdata.user_profile);
    console.log(this.utility.localdata.user_profile)
    
  }

 selectChild(child){
    const parm = {
      responsible_name : this.data.responsible_name,
      facebook_id : this.data.facebook_id,
      access_code : this.data.access_code,
      phone_number : this.data.phone_number,
      child : child
    }
    console.log(parm);
    this.utility.localdata.selectChild = child;
    this.utility.SetLocalStore('selectChild',child);

    this.navCtrl.setRoot('ChildernDashboardPage',{parm : parm})
  }

  addChild = () =>{
    let user_profile = this.utility.localdata.user_profile
    this.navCtrl.push('Ragistersetep2Page',{data : user_profile});
  }
}


