import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CredantialloginPage } from './credantiallogin';

@NgModule({
  declarations: [
    CredantialloginPage,
  ],
  imports: [
    IonicPageModule.forChild(CredantialloginPage),
  ],
})
export class CredantialloginPageModule {}
