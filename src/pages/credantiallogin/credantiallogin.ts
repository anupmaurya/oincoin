import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiServiceProvider } from '../../providers/apiService/apiService';
import { AppServiceProvider } from '../../providers/appService/appService'
/**
 * Generated class for the CredantialloginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-credantiallogin',
  templateUrl: 'credantiallogin.html',
})
export class CredantialloginPage {
  public authForm: FormGroup;
  constructor(public navCtrl: NavController, 
    public formBuilder: FormBuilder,
    public apiService : ApiServiceProvider,
    public appServiceProvider : AppServiceProvider,
    public navParams: NavParams) {
    this.authForm = formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(3)])]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CredantialloginPage');
  }

  onSubmit(user: any): void {
    if (this.authForm.valid) {
      console.log(this.authForm.value);
      const payload = {
        "login_type":2,	
        "email":this.authForm.value.email,	
        "password":this.authForm.value.password,	
      }
       this.apiService.authantication('login','post',payload).subscribe((res:any) =>{
        this.navCtrl.setRoot('HomePage', {data : res.success.response_data});
       },error => {
        this.appServiceProvider.presentAlert('Message','invalid email and password','cancle');
       })
    
    } else {
      console.log(this.authForm.controls.email)
    }
  }

  goToback(){
    this.navCtrl.pop();
  }

}
