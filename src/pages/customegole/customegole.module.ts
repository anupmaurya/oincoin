import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CustomegolePage } from './customegole';

@NgModule({
  declarations: [
    CustomegolePage,
  ],
  imports: [
    IonicPageModule.forChild(CustomegolePage),
  ],
})
export class CustomegolePageModule {}
