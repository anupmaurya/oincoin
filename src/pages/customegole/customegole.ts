import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { AppServiceProvider } from '../../providers/appService/appService';
import { ApiServiceProvider } from '../../providers/apiService/apiService';

@IonicPage()
@Component({
  selector: 'page-customegole',
  templateUrl: 'customegole.html',
})
export class CustomegolePage {

  public goal_name: string = '';
  public goal_value = 50;
  public number_of_months = 2;
  public id;
  constructor(public navCtrl: NavController,
    public apiServiceProvider: ApiServiceProvider,
    public events: Events,
    public utility: AppServiceProvider,
    public navParams: NavParams) {
    let parm = this.navParams.get('Goal');
    let listGoal = this.navParams.get('listGoal');
    console.log(listGoal)
    if (parm) {
      console.log(parm);
      this.goal_name = parm.goal_name;
      this.id = parm.id;
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CustomegolePage');
  }

  goToback() {
    this.navCtrl.pop();
  }

  goTonext() {
    if (this.id) {
      this.SelectChildGoal(this.id)
    } else {
      let GoalList = this.navParams.get('listGoal');
      let selectGoal;
      for (let i = 0; i < GoalList.length; i++) {
        if (GoalList[i].goal_name.toLowerCase() == this.goal_name.toLowerCase()) {
          selectGoal = GoalList[i];
        }
      }
      if (!selectGoal) {
        let data = {
          "goal_name": this.goal_name,
          "user_id": this.utility.localdata.user_profile.id
        }
        this.apiServiceProvider.callApi('addGoal', 'post', data).subscribe(res => {
          if (res.success) {
            console.log(res.success)
            let data = {  
              "goal_id":res.success.id,
              "goal_name": this.goal_name,
              "goal_value": this.goal_value,
              "number_of_months": this.number_of_months,
              "user_id": this.utility.localdata.user_profile.id,
            }

            let GoalList = this.navParams.get('listGoal');
            GoalList.push({  goal_icon: null, 
                             goal_name: this.goal_name, 
                             goal_value: this.goal_value,
                             id:res.success.id
                          });

            this.events.publish('SelectGoal',data,GoalList)
            this.navCtrl.pop();
          }
        }, error => {
          console.log(error);
        })
      }else{
          this.SelectChildGoal(selectGoal.id);
      }
    }
  }

  SelectChildGoal(id){
    
    let data = {  
      "goal_id":id,
      "goal_name": this.goal_name,
      "goal_value": this.goal_value,
      "number_of_months": this.number_of_months,
      "user_id": this.utility.localdata.user_profile.id
    }
    console.log(data);  
    this.events.publish('SelectGoal',data)
    this.navCtrl.pop();
  }

  showreupees(value, month) {
    let values = Math.round(value / month)
    return values;
  }

}
