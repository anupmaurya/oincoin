import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CustometaskPage } from './custometask';
import { WeekendlistComponent } from '../../components/weekendlist/weekendlist'

@NgModule({
  declarations: [
    CustometaskPage,
    WeekendlistComponent
  ],
  imports: [
    IonicPageModule.forChild(CustometaskPage),
  ],
})
export class CustometaskPageModule {}
