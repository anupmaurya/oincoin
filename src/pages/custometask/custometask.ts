import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/apiService/apiService';
import { AppServiceProvider } from '../../providers/appService/appService';

@IonicPage()
@Component({
  selector: 'page-custometask',
  templateUrl: 'custometask.html',
})
export class CustometaskPage {

  public dataList = [];
  public parmTaskList = [];
  constructor(public navCtrl: NavController,
    public utility: AppServiceProvider,
    public events: Events,
    public apiServiceProvider: ApiServiceProvider,
    public navParams: NavParams) {
  }


  ionViewDidLoad() {
    if (this.navParams.get('selectTask')) {
      this.dataList = [this.navParams.get('selectTask')];
      console.log(this.navParams.get('listTask'));
    } else {
      let ListTask = this.navParams.get('listTask')
      let count = 0;
      for (var i = 0; i < ListTask.length; i++) {
        if (ListTask[i].isSelect) {
          count = count + 1;
        }
      }
      for (count; count < 3; count++) {
        let object = { task_name: '', day_of_week: '', periodicity_type: "", isSelect: false, id: null }
        this.dataList.push(object);
      }
    }
  }

  selectWeek(weekID, index) {
    console.log(weekID,index);
    this.dataList[index].day_of_week = weekID.selectweek==7 ? '' : weekID.selectweek ;
    this.dataList[index].periodicity_type = weekID.selectweek==7 ? 'D' : 'W' ;
    this.dataList[index].isSelect = true;
    if (!this.dataList[index].id) {
      console.log('isNew');
      this.parmTaskList = this.navParams.get('listTask');
      let isSelect = null
      console.log(this.parmTaskList)
      for (let i = 0; i < this.parmTaskList.length; i++) {
        console.log(this.parmTaskList[i].task_name + "" + this.dataList[index].task_name)
        if (this.parmTaskList[i].task_name.toLowerCase() == this.dataList[index].task_name.toLowerCase()) {
          this.parmTaskList[i].isSelect = true;
          isSelect = this.parmTaskList[i];
        }
      }
      if (!isSelect) {
        let data = {
          "task_name": this.dataList[index].task_name,
          "user_id": this.utility.localdata.user_profile.id
        }
        this.apiServiceProvider.callApi('addTask', 'post', data).subscribe(res => {
          if (res.success) {
            console.log(res.success);
            let data = {
              task_name: res.success.task_name,
              day_of_weekdata: weekID.selectweek==7 ? '' : weekID.selectweek ,
              periodicity_type: weekID.selectweek==7 ? 'D' : 'W' ,
              isSelect: true,
              id: res.success.id
            }
            this.parmTaskList.push(data)
          }
        }, error => {
          console.log(error);
        })
      } else {

      }
    }
  }

  goToback() {
    this.navCtrl.pop();
  }

  goTonext() {
    if (this.parmTaskList.length != 0) {
      this.events.publish('SelectTask', this.parmTaskList);
    } else {
      let payload = {
        custom_task: this.dataList,
        user_id: this.utility.localdata.user_profile.id
      }
      this.events.publish('SelectTask', payload);
    }
    this.navCtrl.pop();
  }

}
