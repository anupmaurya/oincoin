import { Component } from '@angular/core';
import { IonicPage ,NavController,NavParams } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/apiService/apiService';
import { AppServiceProvider } from '../../providers/appService/appService';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController,
    public utility : AppServiceProvider,
    public navParams: NavParams,
    public apiServiceProvider : ApiServiceProvider
  ) {
      console.log(this.utility.localdata.user_profile)
  }

  openPage(type){
    let parm;
    if(this.utility.localdata.user_profile){
      parm = this.utility.localdata.user_profile
    }else{
      parm = this.navParams.get('data');
    }
    
    if(type==1){
      this.utility.localdata.isparent = true
      this.utility.SetLocalStore('isparent',true);
      this.navCtrl.setRoot('ChildernListPage', {data : parm});
    }else{
      this.utility.localdata.isparent = false
      this.utility.SetLocalStore('isparent',false);
      this.navCtrl.setRoot('ChildernListPage', {data : parm});
    }
  }
}

