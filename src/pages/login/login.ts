import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/apiService/apiService';
import { AppServiceProvider } from '../../providers/appService/appService'

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public appServiceProvider : AppServiceProvider,
    public apiServiceProvider :  ApiServiceProvider ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  onInputkey(e){
    if(e.target.value.length==4){
      this.appServiceProvider.ShowLoader()
      console.log(e.target.value);
      let pro = {
        "login_type":1,
        "access_code":e.target.value,
      }
      this.apiServiceProvider.authantication('login','post',pro).subscribe(res=>{
        this.appServiceProvider.HideLoader()
        // 
        this.navCtrl.setRoot('HomePage', {data : res.success.response_data});

      },error=>{
        this.appServiceProvider.HideLoader()
        this.appServiceProvider.presentAlert('Message','wrong code! please try again','cancle');
      })
      //this.navCtrl.setRoot('ChildernListPage');
    }
  }
  
  signupPage(){
    this.navCtrl.push('SignupPage')
  }

  otherLoign(){
    this.navCtrl.push('OtherloginPage')
  }

}
