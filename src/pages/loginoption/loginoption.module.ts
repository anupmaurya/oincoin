import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginoptionPage } from './loginoption';

@NgModule({
  declarations: [
    LoginoptionPage,
  ],
  imports: [
    IonicPageModule.forChild(LoginoptionPage),
  ],
})
export class LoginoptionPageModule {}
