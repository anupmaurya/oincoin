import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ObjectiveeditPage } from './objectiveedit';

@NgModule({
  declarations: [
    ObjectiveeditPage,
  ],
  imports: [
    IonicPageModule.forChild(ObjectiveeditPage),
  ],
})
export class ObjectiveeditPageModule {}
