import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Events } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/apiService/apiService';
import { AppServiceProvider } from '../../providers/appService/appService';


@IonicPage()
@Component({
  selector: 'page-objectiveedit',
  templateUrl: 'objectiveedit.html',
})
export class ObjectiveeditPage {

  public data = {
    children_id : '',
    children_name : '',
    child_earn : 0,
    goal_icon : '',
    gender : '',
    child_goal_id : '',
    goal_id : '',
    goal_value : '0',
    number_of_months : '',
    created_at : '',
    goal_name : '',
    ischild_new : true,
    isparent : true,
    child_progress :'0%',
    panding_earn : '0'
  }

  
  constructor(public navCtrl: NavController, 
    public utility : AppServiceProvider,public events: Events,
    public apiServiceProvider : ApiServiceProvider,
    public navParams: NavParams) {
    console.log(this.navParams.get('data'));
    this.data = this.navParams.get('data');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ObjectiveeditPage');
  }

  removeGoal(){
    const payload = {
      child_goal_id : this.data.child_goal_id
    }
    this.apiServiceProvider.callApi('deleteTasksandGoal','post',payload).subscribe(res=>{
       this.navCtrl.setRoot('ChildernDashboardPage')
    },error=>{
     
    })
  }

  editGoal = () => {
    this.navCtrl.setRoot('ChildernDashboardPage')
  }

  changeImg = () => {
    console.log(this.data);
    console.log('editGoal');
    let id  : any = this.data.goal_id
    if(id!=1 && id!=2 && id!=3 ){
      this.utility.showActionSheet('picture').then((res: any)=>{
        console.log(res);
        let file: File = res;
        let formData: FormData = new FormData();
        formData.append('goal_id', this.data.goal_id);
        formData.append('goal_icon', file);
         this.apiServiceProvider.callApi('updateGoalIcon','post',formData).subscribe((res)=>{
           console.log(res);
           this.data.goal_icon = res.success;
         },error =>{
           alert(JSON.stringify(error))
         })
       });
    }else{
      this.utility.presentAlert('Message','This goal is not custome','Dismiss');
    }
    
  }

  
  
}

