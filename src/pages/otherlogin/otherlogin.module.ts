import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OtherloginPage } from './otherlogin';
import { Facebook} from '@ionic-native/facebook';

@NgModule({
  declarations: [
    OtherloginPage,
  ],
  imports: [
    IonicPageModule.forChild(OtherloginPage),
  ],
  providers: [
    Facebook
  ]
})
export class OtherloginPageModule {}
