import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
//import { AppServiceProvider } from '../../providers/appService/appService'
import { ApiServiceProvider } from '../../providers/apiService/apiService'


@IonicPage()
@Component({
  selector: 'page-otherlogin',
  templateUrl: 'otherlogin.html',
})
export class OtherloginPage {

  constructor(
    public navCtrl: NavController, 
    private fb: Facebook,
    public apiServiceProvider : ApiServiceProvider,
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OtherloginPage');
  }

  FacebookTologin(){
    this.fb.login(['public_profile','email'])
    .then((res: FacebookLoginResponse) => this.getfbData(res))
    .catch(e =>  this.fb.logout());
  }

  getfbData(data) {
    console.log(data)
    let params = Array();
    this.fb.api("/me?fields=name,gender,email,birthday,first_name,last_name", params)
      .then((user) =>
        this.sigupwithfacebook(user)
      ).catch(e =>  this.fb.logout());
  }

  sigupwithfacebook(value) {
    const payload = {
      "login_type":3,
      "facebook_id": value.id
     }
     this.apiServiceProvider.authantication('login','Post',payload).subscribe((res=>{
        if(res.success.is_new==1){
          this.navCtrl.push('Ragistersetep2Page', {data : value.name,facebook_id : value.id});
        }else{
          this.navCtrl.push('HomePage', {data : res.success.response_data});
        }
       console.log(res)
     }),error=>{
       console.log(error);
     })
  }

  CredantialTologin(){
    this.navCtrl.push('CredantialloginPage')
  }


}
