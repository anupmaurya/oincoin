import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Ragistersetep2Page } from './ragistersetep2';

@NgModule({
  declarations: [
    Ragistersetep2Page,
  ],
  imports: [
    IonicPageModule.forChild(Ragistersetep2Page),
  ],
})
export class Ragistersetep2PageModule {}
