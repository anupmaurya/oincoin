import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-ragistersetep2',
  templateUrl: 'ragistersetep2.html',
})
export class Ragistersetep2Page {

  public data = {
    responsible_name : '',
    number_child : 3
  }
  public facebook_id : any;
  public number_child = 3;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Ragistersetep2Page');
    console.log(this.navParams.get('data').id);
    if(!this.navParams.get('data').id){
      var parm = this.navParams.get('data');
      this.data.responsible_name = parm;
      this.facebook_id = this.navParams.get('facebook_id');
    }
  }

  goToback(){
    this.navCtrl.pop();
  }

  gotoNext(){
    let data ;
    if(this.navParams.get('data').id){
      data = this.navParams.get('data');
      data.number_child = this.number_child;
    }else{
       data ={
        responsible_name : this.data.responsible_name,
        number_child :  this.number_child,
        facebook_id : this.facebook_id
      }
    }

    console.log(data);
    
    this.navCtrl.push('Ragistersetep3Page',{parm : data});
  }

  updateChild(e){
    console.log(this.number_child)
  }
  

}

