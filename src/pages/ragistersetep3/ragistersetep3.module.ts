import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Ragistersetep3Page } from './ragistersetep3';

@NgModule({
  declarations: [
    Ragistersetep3Page,
  ],
  imports: [
    IonicPageModule.forChild(Ragistersetep3Page),
  ],
})
export class Ragistersetep3PageModule {}
