import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/apiService/apiService';
import { AppServiceProvider } from '../../providers/appService/appService';

@IonicPage()
@Component({
  selector: 'page-ragistersetep3',
  templateUrl: 'ragistersetep3.html',
})
export class Ragistersetep3Page {

  public parm = {
    responsible_name : '',
    number_child : null,
    children_data : [],
    facebook_id : null,
    id : null
  };

  constructor(public navCtrl: NavController,
    public utility : AppServiceProvider,
    public apiServiceProvider : ApiServiceProvider, 
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    var parm = this.navParams.get('parm');
    this.parm.responsible_name = parm.responsible_name;
    this.parm.number_child = parm.number_child;
    this.parm.facebook_id = parm.facebook_id;
    this.parm.id = parm.id || null;
    for(var i=0;i<this.parm.number_child;i++){
      var data ={
        name : '',
        gender : 0,
        id : i
      }
      this.parm.children_data.push(data)
    }
  }

  goToback(){
    this.navCtrl.pop();
  }

  gotoNext(){
    console.log(this.parm);
    if(this.parm.id){
      const payload = {
        parent_id : this.parm.id,
        children_data : this.parm.children_data
      }
      this.apiServiceProvider.callApi('addNewChild','post',payload).subscribe(res=>{
        console.log(res);
        this.utility.localdata.user_profile.children_data = res.success;
        this.utility.SetLocalStore('user_profile',this.utility.localdata.user_profile);
        this.navCtrl.setRoot('ChildernListPage');
      },error=>{
        console.log(error);
      })
    }else{
      this.navCtrl.push('Ragistersetep4Page',{parm : this.parm});
    }
  }
  
}
