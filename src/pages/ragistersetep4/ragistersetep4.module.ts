import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Ragistersetep4Page } from './ragistersetep4';

@NgModule({
  declarations: [
    Ragistersetep4Page,
  ],
  imports: [
    IonicPageModule.forChild(Ragistersetep4Page),
  ],
})
export class Ragistersetep4PageModule {}
