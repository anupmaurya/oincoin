import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiServiceProvider} from '../../providers/apiService/apiService'
import { AppServiceProvider } from '../../providers/appService/appService'

@IonicPage()
@Component({
  selector: 'page-ragistersetep4',
  templateUrl: 'ragistersetep4.html',
})
export class Ragistersetep4Page {

  public parm : any={
    number_child : '',
    responsible_name : '',
    children_data : '',
    email : '',
    password :''
  };
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public appServiceProvider:AppServiceProvider,
    public apiServiceProvider : ApiServiceProvider) {
    console.log(this.navParams.get('parm'))
    this.parm = this.navParams.get('parm');
    this.parm.email = '';
    this.parm.password = '';
    this.parm.phone_number = ''
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Ragistersetep4Page');
  }

  goToback(){
    this.navCtrl.pop();
  }

  gotoNext(){
    if(this.parm.responsible_name!='' && this.parm.responsible_name){
      this.apiServiceProvider.callApi('register','post',this.parm).subscribe(res=>{
        if(!res.error){
          this.navCtrl.setRoot('Ragistersetep5Page',{parm : res.success.access_code})
        }else{
          this.appServiceProvider.presentAlert('Message',res.error,'Cancle')
        }
      },err=>{
        console.log(err);
      })
    }else{
      this.appServiceProvider.presentAlert('Message','Please Enter the responsible name','Cancle')
    }
   
  }

}
