import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Ragistersetep5Page } from './ragistersetep5';

@NgModule({
  declarations: [
    Ragistersetep5Page,
  ],
  imports: [
    IonicPageModule.forChild(Ragistersetep5Page),
  ],
})
export class Ragistersetep5PageModule {}
