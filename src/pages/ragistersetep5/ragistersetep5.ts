import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the Ragistersetep5Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ragistersetep5',
  templateUrl: 'ragistersetep5.html',
})
export class Ragistersetep5Page {
  public access_code : any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Ragistersetep5Page');
    console.log(this.navParams.get('parm'))
   this.access_code = this.navParams.get('parm');
  }

  goTo(){
    this.navCtrl.setRoot('LoginPage');
  }

 
}
