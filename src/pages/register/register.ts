import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  public responsible_name : string;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  goToback(){
    this.navCtrl.pop();
  }

  gotoNext(){
    if(this.responsible_name){
      this.navCtrl.push('Ragistersetep2Page',{data : this.responsible_name});
    }else{
      alert('Plase enter Responsible Name')
    }
  }

}
