import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ScheduletaskPage } from './scheduletask';

@NgModule({
  declarations: [
    ScheduletaskPage,
  ],
  imports: [
    IonicPageModule.forChild(ScheduletaskPage),
  ],
})
export class ScheduletaskPageModule {}
