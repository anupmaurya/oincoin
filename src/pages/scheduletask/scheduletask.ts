import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/apiService/apiService';
import { AppServiceProvider } from '../../providers/appService/appService';
// import { DatePicker } from '@ionic-native/date-picker';
import moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-scheduletask',
  templateUrl: 'scheduletask.html',
})
export class ScheduletaskPage {
  public parm = [];
  public childinfo = {
    gender:null,
    children_id:null,
    children_name:"",
    goal_name : "",
    goal_value : "",
    number_of_months : ""
  }
  public currentDate = new Date();
  public showDate = '';
  public isParent = false;
  constructor(public navCtrl: NavController, 
    public utility : AppServiceProvider,
    //private datePicker: DatePicker,
    public apiServiceProvider : ApiServiceProvider,
    public navParams: NavParams) {
      console.log(this.utility.localdata.childinfo);
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ScheduletaskPage');
    console.log(this.utility.localdata.childinfo);
    if(this.utility.localdata.childinfo){
      this.childinfo = this.utility.localdata.childinfo;
      this.showDate = moment(this.currentDate).format('LL'); 
      this.isParent = this.utility.localdata.isparent;
      this.loadTaskAPI(new Date());
    }
   
  }

  isApproved(item,type,i){
    let is_parent_approved;
    let approved_id;
    let isCheck
    if(this.isParent && type=='P' && item.approved_id && item.is_approved==1){
       console.log(item);
       this.parm[i].is_approved = this.parm[i].is_approved==1 ? 2 : 0; 
       is_parent_approved=1; 
       console.log(item);
       approved_id = item.approved_id;
       isCheck = true
    }

    if(!this.isParent && type=='Ch' && !item.approved_id && item.is_approved==0){
       this.parm[i].is_approved = this.parm[i].is_approved==0 ? 1 : 0
       is_parent_approved=0;   
       approved_id = "";
       isCheck = true
    }
    let payload = {
      "children_id":12,
      "task_value":item.task_value,
      "child_goal_id" : this.utility.localdata.childinfo.child_goal_id,
      "task_id":item.task_id,
      "is_parent_approved":is_parent_approved,
      "task_date_time":this.getUtCtime(this.currentDate),
      "approved_id":approved_id 
     }
     
     console.log(isCheck);
     if(isCheck){
       this.apiServiceProvider.callApi('approveTask','post',payload).subscribe(res=>{
        console.log(res);
       },error=>{
        console.log(error);
       })
     }
      
     
  }

  isLoadDateTime(){
    let isDate = new Date();
    let data = moment(isDate).format('YYYYMMDD');
    let time = moment(isDate).format('HHmmss');
    console.log('today is: ', data + ' and time: ', time);
    // this.datePicker.show({
    //   date: new Date(),
    //   mode: 'date',
    //   androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    // }).then(
    //   date => console.log('Got date: ', date),
    //   err => console.log('Error occurred while getting date: ', err)
    // );
  }

  Nextdate(){
    this.currentDate.setDate(this.currentDate.getDate() + 1);
    var date = moment((this.currentDate)).utc().format("YYYY-MM-DD HH:mm:ss").toString();
    this.showDate = moment(date).format('LL'); 
    this.loadTaskAPI(this.currentDate);
  };

  Predate(){
    this.currentDate.setDate(this.currentDate.getDate() - 1);
    var date = moment((this.currentDate)).utc().format("YYYY-MM-DD HH:mm:ss").toString();
    this.showDate = moment(date).format('LL'); 
    this.loadTaskAPI(this.currentDate);
  };

  getUtCtime(nowDate){
    //nowDate.setHours(0,0,0,0)
    var date = moment((nowDate)).utc().format("YYYY-MM-DD HH:mm:ss").toString();
    return date;
  }

  loadTaskAPI(date){
    console.log(this.utility.localdata)
    let payload = {
      "children_id":this.utility.localdata.selectChild.id,
      "date":this.getUtCtime(date),
      "child_goal_id" : this.utility.localdata.childinfo.child_goal_id

    }

    this.apiServiceProvider.callApi('getTaskForApprove','post',payload).subscribe(res=>{
      console.log(res);
      if(res.success){
        if(res.success.length!=0){
          this.parm = res.success;
        }else{
          this.parm = [];
        }
      }else{
        console.log(res.error);
      }
      
    },error=>{
      console.log(error);
    })
  }

  removeGoal(){
    const payload = {
      child_goal_id : this.utility.localdata.childinfo.child_goal_id,
    }
    this.apiServiceProvider.callApi('deleteTasksandGoal','post',payload).subscribe(res=>{
       this.navCtrl.setRoot('ChildernDashboardPage')
    },error=>{
     
    })
  }

  editTask(){
    this.navCtrl.setRoot('ChildernDashboardPage')
  }
}



