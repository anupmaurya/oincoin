import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from "@angular/http";
import { Observable } from 'rxjs/Observable';
import { AppServiceProvider } from '../appService/appService'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';


@Injectable()
export class ApiServiceProvider {
     appUrl = 'http://ec2-34-229-141-135.compute-1.amazonaws.com/index.php/api/';
    // appUrl = 'http://127.0.0.1:8000/api/';

    
  constructor(public http: Http,public appServiceProvider : AppServiceProvider) {}

  callApi(url, method, postParams): any {
      var headers = new Headers();
      console.log(this.appServiceProvider.localdata.tokenKey )
      let token = 'Bearer '+ this.appServiceProvider.localdata.tokenKey;
      headers.append('Authorization', token);
      if(url!='updateGoalIcon'){
        headers.set('Accept', 'application/json');
      }
      
      let options = new RequestOptions({ headers: headers });
      console.log(this.appUrl + url)
      switch (method) {
          case 'post': {
              return this.http.post(this.appUrl + url, postParams, options)
                  .map((res: Response) => res.json())
                  .do(data => {
                      console.log(this.appUrl + url)
                      console.log(data)
                  })
                  .catch(this.handleError);
          }
          case 'get': {
              return this.http.get(this.appUrl + url, options)
                  .map((res: Response) => res.json())
                  .do(data => {
                      console.log(this.appUrl + url)
                      console.log(data)
                  })
                  .catch(this.handleError);
          }
          default: {
              return this.http.post(this.appUrl + url, postParams, options)
                  .map((res: Response) => res.json())
                  .do(data => console.log(JSON.stringify(data)))
                  .catch(this.handleError);
          }
      }
  }

  authantication(url, method, postParams) : any {      
    return this.http.post(this.appUrl + url, postParams)
    .map((res: Response) => res.json())
    .do(data => {
         if(data.success.token){
          this.appServiceProvider.localdata.user_profile = data.success.response_data;
          this.appServiceProvider.SetLocalStore('user_profile',data.success.response_data);
          this.appServiceProvider.SetLocalStore('tokenKey',data.success.token);
          
        } 
    })
    .catch(this.handleError);
   }


  private handleError(error: Response) {
      return Observable.throw(error.json().error());
  }


}
