import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { LoadingController, 
         AlertController, 
         ModalController, 
         ActionSheetController 
        } from 'ionic-angular';

@Injectable()
export class AppServiceProvider {
  public loading;
  public alert;
  public myModal;
  public Userprofile: any;
  
  public localdata = {
    tokenKey : null,
    isparent : null,
    selectChild : null,
    user_profile : null,
    childinfo : null
  };

  constructor( 
    public loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    public modalCtrl: ModalController,
    private storage: Storage,
    private camera: Camera,
    public actionSheetCtrl: ActionSheetController,) {
    console.log('Hello AppServiceProvider Provider');
    this.OnloadLocal();
  }

  public ShowLoader = (): any => {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
   }

  
   public HideLoader = (): any => {
    this.loading.dismiss();
  }

  public presentAlert(title, content, btntxt) {
    return new Promise((resolve, reject) => {
      let alert = this.alertCtrl.create({
        title: title,
        subTitle: content,
        buttons: [{
          text: btntxt,
          role: 'cancel',
          handler: () => {
            resolve(true)
          }
        }]
      });
      alert.present();
    })
  }

  public ShowModel(page, data?) {
    this.myModal = this.modalCtrl.create(page, { data: data }, { cssClass: 'fade-in-right-item' });
    this.myModal.present();
    console.log(this.myModal)
  }

  public closeModel = (): any => {
    console.log(this.myModal)
    this.myModal.dismiss();
  }

  public SetLocalStore(key, data) {
    this.storage.set(key, data).then((res) => {
      this.OnloadLocal()
    });
  }

  public getLocaStore = (): any => {
    return this.localdata;
  }

  public OnloadLocal = (): any => {
    return new Promise((resolve, reject) => {
    this.storage.keys().then((key) => {
      if(key.length!=0){
        for (let i = 0; i < key.length; i++) {
          this.storage.get(key[i]).then((val) => {
            this.localdata[key[i]] = val;
            if(i==(key.length-1)){
              resolve(true);
            }
          })
        }  
      }else{
        resolve(true);
      }})
  })
  }

  public Logout = () => {
    return new Promise((resolve, reject) => {
      this.storage.clear();
      this.localdata = {
        tokenKey : null,
        isparent : null,
        selectChild : null,
        user_profile : null,
        childinfo : null
      };
      resolve(true)
    })
  }

  public showActionSheet(actionType) {
    return new Promise((resolve, reject) => {
      let data;
      if (actionType == 'picture') {
        data = {
          title: 'Change Photo',
          buttons: [
            {
              text: 'Gallery',
              icon: 'ion-images',
              handler: () => {
                this.showCamera('Gallery').then((res) => {
                  resolve(res);
                });
              }
            },{
              text: 'Cancel',
              role: 'cancel',
              icon: 'ion-close',
              handler: () => {
                console.log('Cancel clicked');
              }
            }
          ]
        }

      }

      let actionSheet = this.actionSheetCtrl.create(data);
      actionSheet.present();
    })
  }

  public showCamera = (type): any => {
    return new Promise((resolve, reject) => {
      let sourceType;
      if (type == 'Camera')
        sourceType = this.camera.PictureSourceType.CAMERA;
      else
        sourceType = this.camera.PictureSourceType.PHOTOLIBRARY;

      const options: CameraOptions = {
        quality: 100,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        sourceType: sourceType
      }

      this.camera.getPicture(options).then((imageData) => {
        let formattedData = 'data:image/jpeg;base64,' + imageData;
        var image = this.dataURItoBlobaU(formattedData);
        image.lastModifiedDate = new Date();
        image.name = this.makeid() + '.jpg';
        resolve(image)
      }, (err) => {
        // Handle error
      });
    })
  }

  public dataURItoBlobaU = (dataURI): any => {
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
      byteString = atob(dataURI.split(',')[1]);
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ia], {
      type: 'image/jpeg'
    });
  }

  public makeid = () => {
    var text = "";
    var possible = "0123456789";
    for (var i = 0; i < 5; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
  }

}
